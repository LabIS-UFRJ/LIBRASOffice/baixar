# LIBRASOffice - BAIXAR/DOWNLOAD

Este repositório tem como objetivo disponibilizar os instaladores do LIBRASOffice e instruções para instalação do mesmo.
Para mais informações sobre a descrição do projeto, visite o repositório de ["Descrição e Wiki"](https://gitlab.com/LabIS-UFRJ/LIBRASOffice/Descricao-e-Wiki)

O LIBRASOffice tem como objetivo adaptar o
LibreOffice para pessoas surdas. Para tal,
propomos o desenvolvimento de uma interface que
traduz os itens de menu e submenus do LibreOffice
para LIBRAS – Linguagem Brasileira de Sinais - através
de uma janela localizada no canto inferior direito da
tela do usuário onde são apresentados os sinais/tradução
referentes à funcionalidade selecionada.


## Requisitos para instalação

**Para Windows**

- Microsoft Windows XP, Vista, Windows 7, Windows 8 ou Windows 10;
- PC compatível com Pentium (são recomendados Pentium III, Athlon ou mais recentes);
- 1GB RAM (recomendada 2GB RAM);
- Até 1.5 GB de espaço disponível em disco;
- 1024x768 de resolução (quanto maior for melhor), com pelo menos 256 cores.
São necessários direitos de administrador para o processo de instalação. 
É recomendada a boa prática de fazer uma cópia de segurança do seu sistema e dados antes de instalar ou remover programas.

**Para GNU/Linux**

- Linux kernel versão 2.6.18 ou superior;
- glibc2 versão 2.5 ou superior;
- gtk versão 2.10.4 ou superior;
- PC compatível com Pentium (recomenda-se Pentium III, Athlon ou mai recentes);
- 256MB RAM (recomendada 512MB RAM);
- Até 1.55GB de espaço disponível em disco;
- X Server com resolução de 1024x768 (quanto mais alta melhor), com pelo menos 256 cores;
- Gnome 2.16 ou superior, com os pacotes gail 1.8.6 e at-spi 1.7 
(requeridos para suportar ferramentas de tecnologia de assistência [AT]), ou outro GUI compatível (tal como KDE, entre outros).

Para algumas funcionalidades do programa - mas não muitas - é necessário o Java. O Java é especialmente necessário para o Base.


### Instalando

**Linux**
> Sistemas baseados no Debian (Ubuntu, KDE Neon, Mint e etc):
  - Clique na pasta "LINUX/BASEADOS NO DEBIAN" e em cima do arquivo "LIBRASOffice-BaseDeb.zip";
  - Logo em seguida, aparecerá um botão (grande) com o texto "Download". Clique em cima para fazer o download;
  - Após concluído o download, extraia o arquivo zip no local de sua preferência e entre na pasta descompactada "LIBRASOffice-BaseDeb";
  - Abra o terminal na pasta e digite o comando: 
  `sudo sh instalador.sh`
  - Pronto! O LIBRASOffice já está instalado! Vá para a aba "escritório" no menu do seu sistema e clique para abrir. Caso queira, clique com o botão direito do mouse em cima do ícone do LIBRASOffice no menu, e adicione um atalho à área de trabalho.
